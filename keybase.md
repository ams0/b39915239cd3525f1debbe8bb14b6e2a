### Keybase proof

I hereby claim:

  * I am ams0 on github.
  * I am avozza (https://keybase.io/avozza) on keybase.
  * I have a public key whose fingerprint is B10E C607 72E9 459D 6B5A  7D84 BEE1 E1EE 7CE6 4FD5

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "0120a9785252a38d69428a5870950d2a449fcec01b7db60cfc7ab36f863fd9b1396f0a",
      "fingerprint": "b10ec60772e9459d6b5a7d84bee1e1ee7ce64fd5",
      "host": "keybase.io",
      "key_id": "bee1e1ee7ce64fd5",
      "kid": "01012db2e863cf9b4511fe4eddbd39ab951a4fa41645bb3912aa7c0cbfcdab260d190a",
      "uid": "c09274a98bea8e81c7ee96a78fe25019",
      "username": "avozza"
    },
    "service": {
      "name": "github",
      "username": "ams0"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "ctime": 1577267438,
  "expire_in": 157680000,
  "prev": "783e18724f50604b67ccfb52678bee4cb849557f5a5f15821675c6459b69a3c3",
  "seqno": 18,
  "tag": "signature"
}
```

with the key [B10E C607 72E9 459D 6B5A  7D84 BEE1 E1EE 7CE6 4FD5](https://keybase.io/avozza), yielding the signature:

```
-----BEGIN PGP MESSAGE-----
Version: Keybase OpenPGP v2.1.3
Comment: https://keybase.io/crypto

yMNiAnicbVJ7UFRVHF5QV2BIkchHpeEdqXjpfd97sBV5zFCZo0ijkOR2zr3nrldg
d927LK9INFRQY0odhMkwmoFkUodwxhILXUygsFEbl0p04lEbDGIoKtgAdZfRf5rO
+ePM73e+7zvf75vz3VMzDEF+Lb29d971rvP4dbr35Rq2zCDvFBHIJhcQcUVEFp4+
cLaMNac5S5WJOIKkaBICQeRojoaMKPOApUXIiQIJOFKmIcsCRcISSSFBRjwpKZIA
EcMrIs8oMkAUA3iFhEQMoahWC3bYHarVqcsiisQSTwoCjQHLAZlHHBRkkUUYU/rG
goR5VpE5nbjVpvkYujkENbxctek9vTBP2/sf/BPfunMZ0Vg3IikAsRxFKZjFsoxk
BkAEOAqyCmQpnuUQYgBFQyhIpIQUSYaI5kmZAtO+c6flJBLQAguBiDAUsUhJAsaA
h4KoYJojKeADathhhTlYR0OXrbAQEsUxhN5zqRL2xfr4zqI6t+ai/+BzNNKHdhbY
fWUeRubHRDNSrbKenI53YYem2qxEHKUjJafqY1KcniAvsIwYQ+B8u+rAZtU63eZF
Ul8xhN2BXbqkIDKYEgWaVTiSJ1nEC5KkIE7n6hNhVkIiCzhOUDjIKRQn0hQvcJIe
DUA8gIzEEL5Rtltturb+lBNadE1NtVihM9eBieJWd+ZMg1+QwTjL3/elDEGBIU8+
2nXD3Knwf7L4vPQClLj3uWv1O16qDTZOvhU5PDP+0M6Io1U90fz+4u+59G2DyjdJ
qz5ZumtU297XYanzHkgcrB4pcZ5jXtzXlj2Mzx8aC0g+HNL95avErz1jcODgeNfr
sRkl0mpP/p6ERaauUG005dTKvrTT/lek+SO14psjO56Z+HDtXfff9QdqLI2xVrNW
Oe8r84ri+R5md9v7zURDUzm1YCj96Me0N7nvYGPIFx/YjieiqJtL++d1nBj8MWjz
jaazj2oqiaS01amt7w0RA9HJhob+a+vzP9uwa1mPY5t0P8A1EdbbFfjL4o0NZ71C
08LWvsCdbabxxW8/uH0mM/PYEm9o+JzABfLn4RGewfQVJ1aXDpsuN0e1PEwIntux
//dLHWrkG3Y4GganyjeVpYZ/HRTfNds7O9X1yqzfuj2FJ29rZ2KJgOc7M7M6muZI
7tqrqQ9x9ibxnvHIy4fJm03dDwISrCcH//RbudZTuYFumTSq567XRzkGwPmKyXda
j2VsGbvwR1jz3VsTlp9Nl66WJne7l6d8W7fuo4zxiPruNRdLayKnot1lqzbWgUWj
6+8N7P2hfU3Exae1tPxJf8LkPPJaRXvSpz0lScdTDN5nTxUkVKX9ZNqTV7p5YfKN
/qHq6lTeENFp/CtnWXzco0a2tMx+OjLyhbgLbmNwuzhSkV9aNXb5FljClReFXrn/
Lx/LwvA=
=+V9x
-----END PGP MESSAGE-----

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/avozza

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id avozza
```
